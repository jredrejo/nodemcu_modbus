# nodemcu_modbus

Ejemplo de servidor(esclavo) de ModBus implementado en un NodeMCU v.2 (ESP8266) junto a un código en Python que hace de maestro leyendo y escribiendo datos en la placa.

Usa WifiManager para la conexión WiFi.
## Compilación del software:

Es necesario usar el IDE de Arduino, preparado para compilar software con el esp8266. Hay instrucciones en español en https://www.naylampmechatronics.com/blog/56_usando-esp8266-con-el-ide-de-arduino.html

Una vez preparado el IDE de Arduino es necesario añadir un archivo llamado credenciales.h con las contraseñas necesarias para conectar a ThingSpeak o al broker mqtt, así como las credenciales del punto de acceso Wifi al que se va a conectar si no se usa la opción (predeterminada) de WiFiManager

En caso de no ver en el terminal serie la dirección IP del dispositivo se puede usar [Network Analyzer](https://play.google.com/store/apps/details?id=net.techet.netanalyzerlite.an)  para localizarlo (se identifica el interfaz de red por *Expressif Inc.*)


Librerías de Arduino que hay que instalar :



* https://github.com/adafruit/DHT-sensor-library
* https://github.com/tzapu/WiFiManager
* https://github.com/emelianov/modbus-esp8266


## Esquema

![](extras/montaje_bb.png)