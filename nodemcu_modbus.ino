// de https://github.com/emelianov/modbus-esp8266
#include <ModbusIP_ESP8266.h>

#include "opciones.h"
#include <WiFiManager.h>
#include <strings_en.h>  // English strings for  WiFiManager
#include <DHT.h>
#include <DHT_U.h>

#include <ESP8266WiFi.h>

WiFiManager wifiManager;

//ModbusIP object
ModbusIP mb;

DHT dht(DHTPin, DHTTYPE);
float t;
float h;
unsigned long tiempoInicial;

void setup() {
  Serial.begin(9600);

  pinMode(redLed, OUTPUT);
  pinMode(whiteLed, OUTPUT);
  pinMode(switchPin, INPUT);
  pinMode(DHTPin, INPUT);
  dht.begin();
  delay(300);

  Serial.println("Conectando ");
  wifiManager.autoConnect("NodeMCUModbus");

  digitalWrite(redLed, redLedState);  //empezamos con el led encendido
  tiempoInicial = millis();

  mb.server();
  mb.addCoil(WHITELED_COIL);
  mb.addCoil(REDLED_COIL);
  mb.addIsts(SWITCH_COIL);
  mb.addHreg(TEMPERATURA_REG);
  mb.addHreg(HUMEDAD_REG);
}

void loop() {
  mb.task();

  if (millis() - tiempoInicial > intervalo) {
    tiempoInicial = millis();
    t = dht.readTemperature();         // Leemos la temperatura
    h = dht.readHumidity();               // Leemos la humedad
    redLedState == LOW ? redLedState = HIGH : redLedState = LOW;
    digitalWrite(redLed, redLedState);
    Serial.print("La temperatura es: ");
    Serial.print(t);
    Serial.print("ºC, la humedad es: ");
    Serial.print(h);
    Serial.print("% y el led rojo está: ");
    Serial.println(redLedState);
    if (digitalRead(switchPin) )
      Serial.println("El pulsador está ON");
    else
      Serial.println("El pulsador está OFF");

    mb.Hreg(TEMPERATURA_REG, (int)(t * 100));
    mb.Hreg(HUMEDAD_REG, (int)(h * 100));

  }


  digitalWrite(whiteLed, mb.Coil(WHITELED_COIL)); //escribe en el led el valor de Modbus
  mb.Coil(REDLED_COIL, redLedState);  // escribe en Modbus el estado del led
  mb.Ists(SWITCH_COIL, digitalRead(switchPin) );
  delay(10);

}
