
#define DHTTYPE DHT11  //Tipo de sensor de Tª ambiente y humedad


// pin digital para conectar el DHT11
const uint8_t DHTPin = 5;  // D1
const uint8_t switchPin = 16;  //D0
int redLedState = HIGH;
const uint8_t redLed = 4;  //D2
const uint8_t whiteLed = 0;  //D3
unsigned long intervalo = 10000; // 10 seg

//Modbus Registers Offsets
const int REDLED_COIL = 100;
const int WHITELED_COIL = 101;
const int SWITCH_COIL = 102;
const int TEMPERATURA_REG = 100;
const int HUMEDAD_REG = 101;
