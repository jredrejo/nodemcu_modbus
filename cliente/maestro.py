
import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_tcp
import logging
import time


def main():
    """main"""
    logger = modbus_tk.utils.create_logger("console", level=logging.DEBUG)

    try:

        # Connect to the slave

        master = modbus_tcp.TcpMaster(host="192.168.0.20")
        master.set_timeout(5.0)
        logger.info("connected")
        logger.info(master.execute(1, cst.READ_HOLDING_REGISTERS, 100, 2))
        logger.info(master.execute(1, cst.READ_DISCRETE_INPUTS, 102, 1))
        logger.info(master.execute(1, cst.READ_COILS, 100, 3))
        master.execute(1, cst.WRITE_SINGLE_COIL, 101, output_value=1)
        time.sleep(2)
        master.execute(1, cst.WRITE_SINGLE_COIL, 101, output_value=0)

        # logger.info(master.execute(1, cst.READ_HOLDING_REGISTERS, 0, 2, data_format='f'))

        # Read and write floats
        # master.execute(1, cst.WRITE_MULTIPLE
        # _REGISTERS, starting_address=0, output_value=[3.14], data_format='>f')
        # logger.info(master.execute(1, cst.READ_HOLDING_REGISTERS, 0, 2, data_format='>f'))

        # send some queries
        # logger.info(master.execute(1, cst.READ_COILS, 0, 10))
        # logger.info(master.execute(1, cst.READ_DISCRETE_INPUTS, 0, 8))
        # logger.info(master.execute(1, cst.READ_INPUT_REGISTERS, 100, 3))
        # logger.info(master.execute(1, cst.READ_HOLDING_REGISTERS, 100, 12))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_COIL, 7, output_value=1))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_REGISTER, 100, output_value=54))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_COILS, 0, output_value=[1, 1, 0, 1, 1, 0, 1, 1]))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_REGISTERS, 100, output_value=xrange(12)))

    except modbus_tk.modbus.ModbusError as exc:
        logger.error("%s- Code=%d", exc, exc.get_exception_code())


if __name__ == "__main__":
    main()